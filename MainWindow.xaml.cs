﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using DragEventArgs = System.Windows.DragEventArgs;

namespace VB6_fixer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        void raise(string prop)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private ObservableCollection<LogItem> logs = new ObservableCollection<LogItem>();
        public ObservableCollection<LogItem> Logs
        { 
            get { return logs; }
            set { logs = value; raise("Logs"); } }

        private Visibility visibility;

        public Visibility ProgressVisibility
        {
            get { return visibility; }
            set { visibility = value; raise("ProgressVisibility"); }
        }

        private int count;

        public int FilesCount
        {
            get { return count; }
            set { count = value; raise("FilesCount"); }
        }

        private int total;

        public int TotalFiles
        {
            get { return total; }
            set { total = value; raise("TotalFiles"); }
        }

        private string file;

        public string ProcessingFile
        {
            get { return file; }
            set { file = value; raise("ProcessingFile"); }
        }

        private ICommand searchCommand;

        public ICommand SearchPath
        {
            get { return searchCommand; }
            set { searchCommand = value;
            raise("SearchPath");
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            VB6FileFixer.StartFix += VB6FileFixer_StartFix;
            VB6FileFixer.FileFixed += VB6FileFixer_FileFixed;
            VB6FileFixer.FixError += VB6FileFixer_FixError;
            VB6FileFixer.EndFix += VB6FileFixer_EndFix;

            Reset();

            SearchPath = new RelayCommand<object>((o) =>
            {
                var ofd = new FolderBrowserDialog()
                {
                    ShowNewFolderButton = true
                };
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    DoFix(ofd.SelectedPath);
                }
            });
        }

        private static void DoFix(string path)
        {
            var bw = new BackgroundWorker();
            bw.DoWork += (s, e) => { VB6FileFixer.Fix(path); };

            bw.RunWorkerAsync();
        }

        void VB6FileFixer_EndFix(string obj)
        {
            Log(Brushes.Blue, "******** FINISHED FIXING PATH {0}", obj);
            Reset();
        }

        void Init(string file, IEnumerable<string> files)
        {
            ProgressVisibility = Visibility.Visible;
            FilesCount = 0;
            TotalFiles = files.Count();
            ProcessingFile = file;
        }

        void Reset()
        {
            ProgressVisibility = Visibility.Collapsed;
            FilesCount = 0;
            TotalFiles = 0;
            ProcessingFile = "";
        }

        void Log(Brush color, string msg)
        {
            this.Dispatcher.Invoke(() =>
            {
                Logs.Add(new LogItem()
                {
                    Text = string.Format("{0:G} - {1}{2}", DateTime.Now, msg, ""),
                    Color = color,
                });
            });
        }

        void Log(Brush color, string format, params object[] parms)
        {
            this.Dispatcher.Invoke(() =>
            {
                var msg = string.Format(format, parms);
                Logs.Add(new LogItem()
                {
                    Text = string.Format("{0:G} - {1}{2}", DateTime.Now, msg, ""),
                    Color = color,
                });
            });
        }

        void VB6FileFixer_StartFix(string fpath, IEnumerable<string> files)
        {
            var arr = files.ToArray();
            Init(fpath, arr);
            Log(Brushes.Blue, "Starting to fix path {0}, {1} total files", fpath, arr.Length);
        }

        void VB6FileFixer_FixError(string arg1, Exception arg2)
        {
            Log(Brushes.Red, "********* ERROR FIXING FILE {0}: {1}", arg1, arg2);
            FilesCount++;
        }

        void VB6FileFixer_FileFixed(string obj)
        {
            Log(Brushes.Green, "Fixed file {0}", obj);
            FilesCount++;
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        private void UIElement_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("FileDrop"))
            {
                var path = (string[])e.Data.GetData("FileDrop");
                if (Directory.Exists(path[0]))
                    DoFix(path[0]);

            }
        }

    }

    public class LogItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        void raise(string prop)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private string text = String.Empty;

        public string Text
        {
            get { return text; }
            set { text = value; raise("Text"); }
        }

        private Brush color = Brushes.Green;

        public Brush Color
        {
            get { return color; }
            set { color = value; raise("Color"); }
        }

    }

}
