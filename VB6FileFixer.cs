﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB6_fixer
{
    public static class VB6FileFixer
    {
        public static event Action<string, IEnumerable<string>> StartFix = delegate { };
        public static event Action<string> FileFixed = delegate { };
        public static event Action<string, Exception> FixError = delegate { };
        public static event Action<string> EndFix = delegate { };

        // *.frm;*.bas;*.ctl;*.ctx;*.vbp;*.vbw;*.csi;*.PDM;*.bm;*.cls
        private static string[] extensions = new[]
        {
            ".frm",
            ".bas",
            ".ctl",
            //".ctx",
            ".vbp",
            ".vbw",
            ".csi",
            ".pdm",
            ".bm",
            ".cls",
        };

        static IEnumerable<string> FindFiles(string path)
        {
            var files = Directory.EnumerateFiles(path);
            foreach (var file in files)
            {
                if (!extensions.Contains(Path.GetExtension(file).ToLower()))
                    continue;

                yield return Path.GetFullPath(file);
            }

            var dirs = Directory.EnumerateDirectories(path);
            foreach (var dir in dirs)
            {
                foreach (var findFile in FindFiles(dir))
                {
                    yield return findFile;
                }
            }
        }

        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }

        static void fixFile(string filePath)
        {
            try
            {
                var encoding = GetEncoding(filePath);
                var txt = File.ReadAllText(filePath, encoding);
                txt = txt.Replace("\n", "\r\n").Replace("\r\r", "\r");
                File.Delete(filePath);
                File.WriteAllText(filePath, txt, encoding);
                FileFixed(filePath);
            }
            catch (Exception e)
            {
                FixError(filePath, e);
            }
        }

        public static void Fix(string filesPath)
        {
            var files = FindFiles(filesPath);
            StartFix(filesPath, files);
            foreach (var file in files)
            {
                fixFile(file);
            }
            EndFix(filesPath);
        }
    }
}
